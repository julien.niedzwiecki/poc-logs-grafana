const {createLogger, transports, format} = require("winston");
const LokiTransport = require("winston-loki");
const express = require('express');
const router = express.Router();
const axios = require('axios');
const winston = require("winston");
const { logFmtFormat } = require("winston-logfmt");
const logfmt = require('logfmt');

/* GET home page. */
router.get('/', (req, res, next) => {


  const urlGrafanaLoki = 'https://logs-prod-006.grafana.net';

  const GRAFANA_USERID = "473502";
  const GRAFANA_PASSWORD = "eyJrIjoiNDkwODRmNjcxYjQ0MjVkNDgyNTE2MmY3YjQwYzRjY2NjMjRlY2I2MCIsIm4iOiJqdWxpZW5uaWVkIiwiaWQiOjgzODk2Nn0="
  const GRAFANA_BASICAUTH = GRAFANA_USERID + ":" + GRAFANA_PASSWORD;

  try {
      // SUCCESS
    const logger = createLogger({
      transports: [new LokiTransport({
        host: urlGrafanaLoki,
        labels: {app: 'segmentio-bridge'},
        basicAuth: GRAFANA_BASICAUTH,
        json: true,
        format: winston.format.json(),
        replaceTimestamp: true,
        onConnectionError: (err) => console.error(err)
      }),
        new transports.Console({format: winston.format.json()})
      ]
    })

    logger.info({
      context: 'validatePayload',
      isPayloadValidate: true
    });

    logger.warn({
      context: 'csvRegister',
      API_key: 'Here the API KEY incomplete !!!!!!!!!!!',
      url_UDC: 'www.urlUdc.com',
      payload: 'HERE the payload'
    })
    //
    logger.error({
      context: 'csvRegister',
      API_key: 'Here the API KEY incomplete',
      url_UDC: 'www.urlUdc.com',
      csv: 'CSV HERE'
    })

    return res.status(200).send({
      isSuccess: true,
      message: 'log success'
    });
  } catch (e) {
    // return a response with an 404 status with the output error
    return res.status(404).send({
      isSuccess: false,
      message: e.message
    })
  }
})

module.exports = router;



